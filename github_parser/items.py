import scrapy
from scrapy.loader.processors import TakeFirst



class GithubParserItem(scrapy.Item):
    url = scrapy.Field(output_processor=TakeFirst())
    extra = scrapy.Field(output_processor=TakeFirst())


class OwnerItem(scrapy.Item):
    owner = scrapy.Field(output_processor=TakeFirst())


class StackItem(scrapy.Item):
    name = scrapy.Field(output_processor=TakeFirst())
    percentage = scrapy.Field(output_processor=TakeFirst())
