import scrapy
import json
import re
from github_parser.items import GithubParserItem, OwnerItem, StackItem

class github_spider(scrapy.Spider):
    name = "github"
    allowed_domains = ["https://github.com"]
    base_url = "https://github.com/search?q={0}&type={1}"

    def start_requests(self):
        input_file = "input_files/" + self.input_file
        text = open(input_file)
        json_input = json.loads(text.read())
        text.close()

        for keyword in json_input["keywords"]:
            request = scrapy.Request(url=self.base_url.format(keyword, json_input["type"]),
                                     callback=self.parse_response)
            yield request


    def parse_response(self, response):
        item_class = response.selector.css("li.repo-list-item")

        languages = []
        #It requires 2 runs. One to collect all the language stats so we can calculate the averages.
        for item in item_class:
            stack = item.css("div.flex-shrink-0 > div.text-gray::text").extract()[-1].strip()
            languages.append(stack)

        language_stats = {}
        for l in set(languages):
            language_stats[l] = languages.count(l)/len(languages) * 100

        print(language_stats)
        for item in item_class:
            link = "https://github.com" + item.css("a::attr(href)").extract_first()
            owner = re.findall("github.com\/(.*)\/", link)[0]
            item = GithubParserItem()
            item["url"] = link
            item["extra"] = {"owner": owner, "language_stats": language_stats}

            yield item
